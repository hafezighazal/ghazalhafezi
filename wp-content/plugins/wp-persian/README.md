# WP-Persian Wordpress Plugin

version: 3.2.5

Tested on: Wordpress 4.9.6 + PHP 7.0.10 + PHP 7.2.5 + Google Chrome 66.0 + Firefox 60.0

Fast and Powerful plugin for Jalali calendar and Farsi language support in Wordpress and standard plugins.


